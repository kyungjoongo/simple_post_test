// @flow
import * as React from 'react';
import {Button, Input, Modal, notification} from "antd";
import {reactLocalStorage} from 'reactjs-localstorage';
import './App.css'

const {TextArea} = Input;
type Props = {};
type State = {
    article: any,
    name: string,
    password: string,
    contents: string,
    postList: any,
    id: string,
    currentIndex: number,
    isModifyMode: boolean,
    confirmPassword: string,
    isModalVisible: boolean,
    isConfirmPasswordCorrect: boolean,
    currentPassword: string,
    isModifyButtonVisible: boolean,
};


export default class App extends React.Component<Props, State> {

    constructor(props) {
        super(props);
        this.state = {
            name: undefined,
            password: undefined,
            contents: undefined,
            id: undefined,
            currentIndex: -1,
            isModifyMode: false,
            postList: reactLocalStorage.getObject('postList') !== undefined ? reactLocalStorage.getObject('postList') : [],
            confirmPassword: undefined,
            isConfirmPasswordCorrect: true,
            currentPassword: undefined,
            isModifyButtonVisible: false,
        }
    }

    componentDidMount(): void {
        //reactLocalStorage.clear()
    }


    saveOnePost() {
        try {
            const {name, password, contents} = this.state;

            if (name === undefined || password === undefined || contents === undefined) {
                this.showToast('please, check empty form!!')
                return false;
            } else {
                let onePost = {
                    id: this.makeUniqueId(),
                    name: this.state.name,
                    password: this.state.password,
                    contents: this.state.contents,
                    regDate: getDateTime(),
                }

                let prevPostList = reactLocalStorage.getObject('postList');

                if (isEmpty(prevPostList)) {
                    prevPostList = []
                }


                if (prevPostList !== {}) {
                    prevPostList.push(onePost)
                    reactLocalStorage.setObject('postList', prevPostList);
                    let newPostList = reactLocalStorage.getObject('postList');
                    this.setState({
                        postList: newPostList,
                        /*name: undefined,
                        contents: undefined,
                        password: undefined,*/
                    }, () => {
                        //this.showToast('have registered one post')
                    })
                } else {
                    //todo : postList initialize
                    reactLocalStorage.setObject('postList', []);
                }
            }

        } catch (e) {

        }
    }

    makeUniqueId() {
        return '_' + Math.random().toString(36).substr(2, 9);
    }

    showToast(msg) {
        notification.success({
            message: msg,
            placement: 'bottomLeft',
            duration: 1.0,
        });
    }

    fillFormForModify(pId) {
        try {
            let postList = reactLocalStorage.getObject('postList');
            const itemIdx = postList.findIndex((item) => {
                return item.id === pId
            })

            let postOne = postList[itemIdx];
            this.setState({
                id: postOne.id,
                name: postOne.name,
                password: postOne.password,
                contents: postOne.contents,
                regDate: getDateTime(),
                currentIndex: itemIdx,
                isModifyMode: true,
                isModalVisible: false,
                isModifyButtonVisible: true,
            })
        } catch (e) {

        }
    }

    modifyPostOne() {

        try {
            let newPostOne = {
                id: this.state.id,
                name: this.state.name,
                password: this.state.password,
                contents: this.state.contents,
                regDate: getDateTime(),

            }
            let postList = reactLocalStorage.getObject('postList');

            postList[this.state.currentIndex] = newPostOne;
            reactLocalStorage.setObject('postList', postList);
            this.setState({
                postList,
                isModifyMode: false,
                isModifyButtonVisible: false,
            })

            this.showToast(this.state.contents + '  수정완료!')
        } catch (e) {

        }


    }

    deletePostOne() {
        try {
            if (!isEmpty(this.state.confirmPassword)) {
                //todo:패스워드가 일치해는 경우에만
                if (this.state.currentPassword === this.state.confirmPassword.trim()) {
                    let postList = reactLocalStorage.getObject('postList');
                    const itemIdx = postList.findIndex((item) => {
                        return item.id === this.state.id
                    })

                    if (itemIdx > -1) {
                        postList.splice(itemIdx, 1)
                    }


                    reactLocalStorage.setObject('postList', postList);
                    this.setState({
                        postList: postList,
                        isModalVisible: false,
                    })
                    this.showToast('게시물 삭제 완료!.')
                } else {
                    this.showToast('패스워드가 일치 하지 않음.')
                }
            } else {
                this.showToast('confirm password is empty')
            }


        } catch (e) {

        }

    }

    renderManipulateButtons() {
        try {
            return (
                <React.Fragment>

                    {this.state.isModifyButtonVisible && <Button style={{backgroundColor: 'orange', borderColor: 'orange', marginRight: 10}} onClick={() => {
                        this.modifyPostOne(this.state.id)

                    }} type="primary">Modify</Button>}
                    <Button style={{marginRight: 10}} onClick={() => {
                        this.saveOnePost();

                    }} type="primary">Register</Button>

                    <Button onClick={() => {
                        reactLocalStorage.clear();
                        this.showToast('have deleted all posts!')
                        this.setState({
                            postList: [],
                            name: '',
                            contents: '',
                            password: undefined,
                        })
                    }} type="danger">Delete all post</Button>
                </React.Fragment>
            )
        } catch (e) {

        }
    }


    renderModal() {
        try {
            return (
                <Modal
                    title="please input password."
                    visible={this.state.isModalVisible}
                    onOk={async () => {

                        if (!isEmpty(this.state.confirmPassword)) {
                            await this.deletePostOne();
                        } else {
                            this.showToast('패스워드를 입력하시오!')
                        }
                    }}
                    onCancel={() => {
                        this.setState({
                            confirmPassword: '',
                            isModalVisible: false,
                        })
                    }}
                >
                    <Input
                        ref={c => this.popupInput = c}
                        value={this.state.confirmPassword}
                        placeholder="confirm_password"
                        onChange={(e) => {
                            this.setState({
                                confirmPassword: e.target.value,
                            });
                        }}
                    />
                    {!this.state.isConfirmPasswordCorrect &&
                    <div style={{color: 'red'}}>
                        password가 틀렸슴다.
                    </div>}
                </Modal>
            )
        } catch (e) {

        }
    }

    renderPostOneDiv(item) {
        try {
            return (
                <div className='tr'>
                    <div style={{fontSize: 17, color: 'black'}}>
                        {item.contents}
                    </div>
                    <div style={{height: 30}}/>
                    <div style={{display: 'flex'}}>
                        <div style={{flex: .33}}>{item.name}</div>
                        <div style={{flex: .33}}>{item.regDate}</div>
                        <div style={{flex: .33}}>
                            <Button
                                style={{backgroundColor: 'green', borderColor: 'green'}}
                                onClick={() => {
                                    this.fillFormForModify(item.id)
                                }}
                                type="primary"
                            >modify
                            </Button>
                            <Button
                                style={{marginLeft: 10,}}
                                onClick={async () => {
                                    await this.setState({
                                        isModalVisible: true,
                                        id: item.id,
                                        currentPassword: item.password,
                                        confirmPassword: undefined,
                                    })

                                    setTimeout(() => {
                                        this.popupInput.focus();
                                    }, 250)

                                }}
                                type="danger"
                            >delete
                            </Button>
                        </div>
                    </div>
                </div>
            )
        } catch (e) {

        }
    }

    renderInputForms() {
        try {
            return (
                <div style={{display: 'flex'}}>
                    <div style={{marginTop: 5}}>
                        name
                    </div>
                    <div style={{marginLeft: 10}}>
                        <Input
                            value={this.state.name}
                            placeholder="name"
                            onChange={(e) => {
                                this.setState({
                                    name: e.target.value,
                                })
                            }}
                        />
                    </div>
                    <div style={{marginTop: 5, marginLeft: 10,}}>
                        password
                    </div>
                    <div style={{marginLeft: 10}}>
                        <Input value={this.state.password} type={'password'} placeholder="password"
                               onChange={(e) => {
                                   this.setState({
                                       password: e.target.value,
                                   })
                               }}
                        />
                    </div>

                </div>
            )
        } catch (e) {

        }
    }

    render() {
        try {
            return (
                <div style={{flex: 1, width: '30%', flexDirection: 'column', margin: 25,}}>
                    {this.renderModal()}
                    {this.renderInputForms()}
                    <div style={{height: 50}}/>
                    <TextArea rows={4}
                              value={this.state.contents}
                              onChange={(e) => {
                                  this.setState({
                                      contents: e.target.value,
                                  })
                              }}
                    />
                    <div style={{height: 50}}/>

                    <div>
                        {this.renderManipulateButtons()}
                    </div>

                    <div style={{height: 50}}/>
                    {this.state.postList.length > 0 && this.state.postList.reverse().map((item, index) => {
                        return (
                            this.renderPostOneDiv(item)
                        )
                    })}

                </div>
            );
        } catch (e) {

        }
    };
};

export function getDateTime() {
    var now = new Date();
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    if (month.toString().length === 1) {
        month = '0' + month;
    }
    if (day.toString().length === 1) {
        day = '0' + day;
    }
    if (hour.toString().length === 1) {
        hour = '0' + hour;
    }
    if (minute.toString().length === 1) {
        minute = '0' + minute;
    }
    if (second.toString().length === 1) {
        second = '0' + second;
    }
    var dateTime = year + '/' + month + '/' + day + ' ' + hour + ':' + minute + ':' + second;
    return dateTime;
}

export const isEmpty = (value) => {
    if (value === "undefined" || value === "" || value === null || value === undefined || (typeof value === "object" && !Object.keys(value).length)) {
        return true
    } else {
        return false
    }
};